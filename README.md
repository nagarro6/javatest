
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Nagarro Java Tests </h3>

  <p align="center">
    JavaEE Test for Rashed A.Mohammed!
  </p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
    </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

The server will handle requests to view statements by performing simple search on date and
amount ranges.
- The request should specify the account id.
- The request can specify from date and to date (the date range).
- The request can specify from amount and to amount (the amount range).
- If the request does not specify any parameter, then the search will return three months
  back statement.
- If the parameters are invalid a proper error message should be sent to user.
- The account number should be hashed before sent to the user.
- All the exceptions should be handled on the server properly.

<h3>SonarQube Report at :</h3>
[https://sonarcloud.io/dashboard?id=nagarro6_javatest](https://sonarcloud.io/dashboard?id=nagarro6_javatest)

### Built With

The Project is Java EE
* [Spring boot](https://spring.io/projects/spring-boot)
* [Angualr](https://angular.io/)

## Getting Started

This is how you can setting up the project locally.
To get a local copy up and running follow these simple  steps.

### Prerequisites
* Java JDK
* Maven Installed
* npm
* Angualr Cli
* git
* IDE (intellij preferred)

### Installation

1. Clone the repo [*master branch]
   ```sh
   git clone https://gitlab.com/nagarro6/javatest.git
   ```
3. Run Maven commands
   ```sh
   mvn clean install
   ```
   ```sh
   mvn spring-boot:run
   ```
   
## Usage
* pleas change DB url in application.properties file.
1) this app use swagger api documentation (* with angualr in branch fullstack)
2) I add users,roles tables to db
3) in swagger-ui there are to API's for this tests (* get API for both User & Admin and post for admin only)
visit [http://localhost:8072](http://localhost:8072) and login with:

username: admin

passowrd: admin

or 

username: user

password: user


<!-- CONTACT -->
## Contact

Rashed A.Mohammed - [Linked In](https://twitter.com/your_username) 

Project Link: [https://gitlab.com/nagarro6/javatest.git](https://gitlab.com/nagarro6/javatest.git)



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
