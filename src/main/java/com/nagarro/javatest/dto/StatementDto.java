package com.nagarro.javatest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatementDto implements Serializable {
    String datefield;
    String amount;
    String accountType;
    String accountNumber;
}
