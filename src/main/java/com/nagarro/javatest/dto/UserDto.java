package com.nagarro.javatest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    String username;
    String password;
    private String[] roles;
}
