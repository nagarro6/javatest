package com.nagarro.javatest.service;

import com.nagarro.javatest.util.RequestInfo;
import com.nagarro.javatest.dto.StatementDto;

import java.util.List;

public interface StatementService {
    List<StatementDto> search(RequestInfo requestInfo);

    List<StatementDto> search(int accountId);
}
