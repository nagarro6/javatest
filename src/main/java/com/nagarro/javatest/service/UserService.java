package com.nagarro.javatest.service;

import com.nagarro.javatest.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> userList();
    UserDto findByUsername(String username);
}
