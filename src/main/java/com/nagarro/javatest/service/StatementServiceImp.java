package com.nagarro.javatest.service;

import com.nagarro.javatest.util.RequestInfo;
import com.nagarro.javatest.dto.StatementDto;
import com.nagarro.javatest.model.StatementEntity;
import com.nagarro.javatest.repository.StatementRepo;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatementServiceImp implements StatementService{
    private static final Logger log = LoggerFactory.getLogger(StatementServiceImp.class);

    @Autowired
    StatementRepo statementRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<StatementDto> search(RequestInfo requestInfo) {
        List<StatementEntity> statements= statementRepo.search(requestInfo);
        log.info("Search Query Result!! {}",statements);
        List<StatementDto> response
                = modelMapper.map(statements, new TypeToken<List<StatementDto>>() {}.getType());
        log.info("Mapped Result!!{}",response);
        return response;
    }

    @Override
    public List<StatementDto> search(int accountId) {
        List<StatementEntity> statements= statementRepo.search(accountId);
        log.info("Search Query Result!! {}",statements);
        List<StatementDto> response
                = modelMapper.map(statements, new TypeToken<List<StatementDto>>() {}.getType());
        log.info("Mapped Result!!{}",response);
        return response;
    }
}
