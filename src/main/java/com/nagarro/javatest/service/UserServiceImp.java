package com.nagarro.javatest.service;

import com.nagarro.javatest.dto.UserDto;
import com.nagarro.javatest.model.UserEntity;
import com.nagarro.javatest.repository.UserRepo;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService{

    @Autowired
    UserRepo userRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<UserDto> userList() {
        List<UserEntity> users=userRepo.findAll();
        return modelMapper.map(users, new TypeToken<List<UserDto>>() {}.getType());
    }

    @Override
    public UserDto findByUsername(String username) {
        UserEntity user=userRepo.findByUsername(username);


        return  modelMapper.map(user, new TypeToken<UserDto>() {}.getType());
    }


}
