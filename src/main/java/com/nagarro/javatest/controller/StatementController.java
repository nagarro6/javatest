package com.nagarro.javatest.controller;

import com.nagarro.javatest.util.RequestInfo;
import com.nagarro.javatest.dto.StatementDto;
import com.nagarro.javatest.service.StatementService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/statement")
@Api(value="statement Controller ")
public class StatementController {
    private static final Logger log = LoggerFactory.getLogger(StatementController.class);

    @Autowired
    StatementService statementService;

    @GetMapping(value="/search")
    public List<StatementDto> search(@RequestParam("accountId") int accountId){
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>search Request For Role User and Admin<<<<<<<<<<<<<<<<<{}",accountId);
        return statementService.search(accountId);
    }

    @PostMapping(value="/search")
    public List<StatementDto> search(@Valid @RequestBody RequestInfo requestInfo){
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>search Request For Role Admin<<<<<<<<<<<<<<<<<{}",requestInfo);
        return statementService.search(requestInfo);
    }
}
