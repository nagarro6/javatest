package com.nagarro.javatest.util;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RequestInfo {

    @NotNull
    @Min(value = 1, message = "invalid account id ")
    int  accountId;
    @NotNull
    @NotBlank
    String dateFrom;
    @NotNull
    @NotBlank
    String dateTo;
    @NotNull
    @NotBlank
    String amountFrom;
    @NotNull
    @NotBlank
    String amountTo;
}
