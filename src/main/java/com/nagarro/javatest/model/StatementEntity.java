package com.nagarro.javatest.model;

import lombok.Data;

@Data
public class StatementEntity {
    Long id;
    int accountId;
    String datefield;
    String amount;
    String accountType;
    String accountNumber;

    public StatementEntity(long id, int accountId, String datefield, String amount,String accountType,String accountNumber) {
        this.id=id;
        this.accountId=accountId;
        this.datefield=datefield;
        this.amount=amount;
        this.accountType=accountType;
        this.accountNumber=accountNumber.substring(0,6)+"***"+accountNumber.substring(accountNumber.length()-4);
    }
}
