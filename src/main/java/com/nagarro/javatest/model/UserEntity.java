package com.nagarro.javatest.model;

import lombok.Data;

@Data
public class UserEntity {

    String username;
    String password;
    private String[] roles;

    public UserEntity(String username, String password,String... roles) {
        this.username=username;
        this.password=password;
        this.roles = roles;
    }

    public UserEntity() {

    }
}
