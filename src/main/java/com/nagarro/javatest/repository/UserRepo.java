package com.nagarro.javatest.repository;

import com.nagarro.javatest.model.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class UserRepo {

    private static final Logger log = LoggerFactory.getLogger(UserRepo.class);


    @Autowired
    private JdbcTemplate template;

    public List<UserEntity> findAll() {
        log.info("Sql query>> START");
        return template.query("select * from users", new RowMapper<UserEntity>() {
            @Override
            public UserEntity mapRow(ResultSet resultSet, int i) throws SQLException {
                return new UserEntity(resultSet.getString("username"),resultSet.getString("password"));
            }
        });
    }

    public UserEntity findByUsername(String username) {
        try {
            log.info("Sql query>> START");
            return template.queryForObject("select * from users LEFT OUTER JOIN roles on roles.id = role_id where username='"+username+"';", new RowMapper<UserEntity>() {
                @Override
                public UserEntity mapRow(ResultSet resultSet, int i) throws SQLException {
                    return  new UserEntity(resultSet.getString("username"),resultSet.getString("password"),resultSet.getString("roles"));
                }
            });
        } catch (EmptyResultDataAccessException e) {
            return new UserEntity();
        }
    }
}
