package com.nagarro.javatest.repository;

import com.nagarro.javatest.model.StatementEntity;
import com.nagarro.javatest.util.RequestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class StatementRepo {

    private static final Logger log = LoggerFactory.getLogger(StatementRepo.class);

    @Autowired
    Environment environment;

    @Autowired
    private JdbcTemplate template;

    public List<StatementEntity> search(RequestInfo requestInfo) {
        int accountId=requestInfo.getAccountId();
        String dateForm=requestInfo.getDateFrom();
        String dateTo=requestInfo.getDateTo();
        String amountFrom=requestInfo.getAmountFrom();
        String amountTo=requestInfo.getAmountTo();

        String query="SELECT * from statement LEFT OUTER JOIN account  on account.id = statement.account_id where  account_id="+accountId
                +" and amount between "+amountFrom+" and "+amountTo+" and" +
                " DateValue(Format(REPLACE(datefield,'.','/'),'dd/MM/yyyy'))\n" +
                " between DateValue(Format(REPLACE('"+dateForm+"','.','/'),'dd/MM/yyyy') )\n" +
                "and DateValue(Format(REPLACE( '"+dateTo+"','.','/'),'dd/MM/yyyy') ) ;";
        log.info("Sql query>> START");
        return template.query(query, new RowMapper<StatementEntity>() {
            @Override
            public StatementEntity mapRow(ResultSet resultSet, int i) throws SQLException {
                return new StatementEntity(resultSet.getLong("ID"),resultSet.getInt("account_id"),resultSet.getString("datefield"),resultSet.getString("amount"),resultSet.getString("account_type"),resultSet.getString("account_number"));
            }
        });
    }


    public List<StatementEntity> search(int accountId) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Calendar c = Calendar.getInstance();
        Calendar c1 = Calendar.getInstance();
        c.setTime(new Date());
        c1.setTime(new Date());
        c.add(Calendar.MONTH, -Integer.parseInt(environment.getProperty("monthBack")));

        Date d = c.getTime();
        Date d1 = c1.getTime();
        String dateForm = format.format(d);
        String dateTo = format.format(d1);

        String query="SELECT * from statement LEFT OUTER JOIN account  on account.id = statement.account_id where  account_id="+accountId+" and  DateValue(Format(REPLACE(datefield,'.','/'),'dd/MM/yyyy'))\n" +
                " between DateValue(Format(REPLACE('"+dateForm+"','.','/'),'dd/MM/yyyy') )\n" +
                "and DateValue(Format(REPLACE( '"+dateTo+"','.','/'),'dd/MM/yyyy') ) ;";
        log.info("Sql query>> START");
        return template.query(query, new RowMapper<StatementEntity>() {
            @Override
            public StatementEntity mapRow(ResultSet resultSet, int i) throws SQLException {
                return new StatementEntity(resultSet.getLong("ID"),resultSet.getInt("account_id"),resultSet.getString("datefield"),resultSet.getString("amount"),resultSet.getString("account_type"),resultSet.getString("account_number"));
            }
        });
    }
}