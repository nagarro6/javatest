package com.nagarro.javatest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class JavaTestApplicationTests {

	private static final Logger log = LoggerFactory.getLogger(JavaTestApplicationTests.class);

	@LocalServerPort
	int randomServerPort;

	/*API TESTS*/

	@Test
	void testStatementSearchSuccess() throws URISyntaxException
	{
		log.info("#######################TEST(testStatementSearchSuccess)START###########################");
		RestTemplate restTemplate = new RestTemplate();
		final String baseUrl = "http://localhost:"+randomServerPort+"/statement/search?accountId="+3;
		URI uri = new URI(baseUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");

		ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

		//Verify request status
		Assertions.assertEquals(200, result.getStatusCodeValue());
	}

	@Test
	void testStatementSearchInvalidAccountId() throws URISyntaxException
	{
		log.info("#######################TEST(testStatementSearchInvalidAccountId)START###########################");
		RestTemplate restTemplate = new RestTemplate();
		final String baseUrl = "http://localhost:"+randomServerPort+"/statement/search?accountId=-1";
		URI uri = new URI(baseUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");

		ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

		//Verify request body
		Assertions.assertEquals(200, result.getStatusCodeValue());
	}
}
